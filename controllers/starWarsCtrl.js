'use strict'
const starWars = require('../integrations/starWars')


function getStarWar(req, res) { 
    starWars.getStarWars((err, data) => {
        if (err) {
            res.status(500).send({ message: 'Error Al mostrar datos'})
        }else if (data == "") {
            res.status(412).send({ message: 'No hay datos' })
        } else {
            res.status(200).send({ starWars: data })
        }
    });
}

module.exports = {
    getStarWar,
}