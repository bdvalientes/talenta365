'use strict'

const starWarsUtil = require('../utils/starWarsUtils/starWarsUtil');
let starWarIntegration = {};

starWarIntegration.getStarWars = (res) => {
    const starships = {    
        name:'',
        model:'',
        manufacturer:'',
        passengers:'',        
    }
    const people = {
            name:'',
            gender:'',
            hair_color:'',
            skin_color:'',
            eye_color:'',
            height:'',
            homeworld: '',
            especie: {
                name: '',
                language:'',
                average_height:'',
            }
    }
    const planets = {
            name: '',
            terrain: '',
            gravity:'',
            diameter:'',
            population: '',
    }
    const dataResponse = {
        name: '',
        planets: {
            name: '',
            terrain: '',
            gravity:'',
            diameter:'',
            population: '',
        },
        people: {
            name:'',
            gender:'',
            hair_color:'',
            skin_color:'',
            eye_color:'',
            height:'',
            homeworld: '',
            especie: {
                name: '',
                language:'',
                average_height:'',
            }
        },
        starships: {
            name:'',
            model:'',
            manufacturer:'',
            passengers:'',
        }
    }
    const objectDataResponse = [];
    starWarsUtil.getStarWars().then(response => {   
        res(null, response);        
    }).catch(err => {
        res(err, null);
    })
    
    
}


module.exports = starWarIntegration;