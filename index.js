'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const config = require('./config')
const auth = require('./middlewares/auth')

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
// permitir las peticiones de todos los origenes
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

require('./routes/starWarsRouter')(app, auth);

app.listen(config.PORT, () => {
    console.log('api Corriendo en en http://' + config.AMBIENT + ':' + config.PORT);
})