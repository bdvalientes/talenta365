const ApiUtils = require('../utils');
const config = require('./ConfigURL');

const API = 'STARWARS';

function getStarWars() {
    return ApiUtils.Api(`${config.GET_STARWARS}`, 'GET', null, API);
}



module.exports = {
    getStarWars,
}
