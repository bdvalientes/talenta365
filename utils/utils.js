'use strict'

const fetch = require("node-fetch");

function sendApi(baseUrlCompleta, type, data ) {
    return new Promise((resolve, reject) => {
        // getBody(type,data)
        fetch(baseUrlCompleta)
            .then((response) => {
                if (response.status !== 200) {
                    reject(response);    
                } else {    
                    resolve(response.json());    
                }
            })
            .catch((error) => {
                reject(error);
            })
            .finally(() => {
                console.log("Petición Finalizada");
            });
    });
}

function getHeader() {
    return {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Content-Disposition': '',
    };    
}

function getBody(type, data) {
    if (type === 'GET' || type ==="DELETE") {
        return {
            method: type,
            headers: getHeader(),
        }
    } else {
        return {
            method: type,
            headers: getHeader(),
            body: JSON.stringify(data),
        }
    }
    
}

function Api(metodo, type, data, api) {
    let baseUrlCompleta = '';
    let promise = null;
    switch (api) {
        case 'STARWARS':
            baseUrlCompleta = `${metodo}`;
            promise = sendApi(baseUrlCompleta, type, data);
            break;
        default:
            console.log(`Error Enviar Petición : ${metodo}`);
            break;
    }
    return promise;
}

module.exports = {
    Api,
}
